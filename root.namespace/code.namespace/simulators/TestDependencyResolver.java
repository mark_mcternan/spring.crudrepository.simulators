package com.datus.simulators;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.datus.domain.Customer;
import com.datus.domain.CustomerOffer;
import com.datus.domain.CustomerOfferAddress;
import com.datus.domain.TruAxisOffer;
import com.datus.domain.TruAxisOfferAddress;
import com.datus.service.CustomerOfferRepository;
import com.datus.service.CustomerRepository;
import com.datus.service.TruAxisOfferRepository;
import com.datus.service.TruAxisService;
import com.datus.util.Constant;

public class TestDependencyResolver{
	
	private final static Map<Class<?>, Object> map = new HashMap<Class<?>, Object>();
	
	/**
	 * static constructor
	 */
	static{
		
		// Arrange - set up repos with MockRepo base class
		MockRepository<Customer, Long> customerRepo = new MockCustomerRepository(MockCustomerRepository.generateCustomers(3));
		MockRepository<CustomerOffer, Long> offerRepo = new MockCustomerOfferRepository(MockCustomerOfferRepository.generateCustomerOffers(21));
		MockRepository<TruAxisOffer, Long> truAxisRepo = new MockTruAxisOfferRepository(MockTruAxisOfferRepository.generateTruAxisOffers(40));
		
		// link data
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
		for(Customer c : customerRepo.findAll()){
			
			System.out.println(String.format(
					"Customer id: %d, token: %s, truaxistoken: %s, long: %s lat: %s", 
					c.getId(), c.getToken(), c.getTruAxisToken(), c.getLongitude(), c.getLatitude()));
			
			for(CustomerOffer o : offerRepo.data.next(7)){
				
				o.setCustomer(c);
				o.setCustomerId(c.getId());
				if(o.getId() % 2 == 0){
					o.setStatus(Constant.NEW);
				}else if(o.getId() %3 == 0){
					o.setStatus(Constant.SENT);
				}else{
					o.setStatus(Constant.REDEEMED);
				}
				
				// attach truAxisOffer to offer
				TruAxisOffer t = truAxisRepo.data.next();
				t.setRedemptionType("CASH");
				
				// attach address info
				Random r = new Random();
				Double latitude = r.nextDouble() * 9 + 30.00;
				Double longitude = r.nextDouble() * 8 - 78.00;
				
				// set TruAxisOfferAddresses
				List<TruAxisOfferAddress> addresses = new ArrayList<TruAxisOfferAddress>();
				addresses.add(new TruAxisOfferAddress(t.getCity(), longitude, latitude));
				t.setAddresses(addresses);
				
				o.setTruAxisOfferId(t.getId());
				o.setTruAxisOffer(t);
				
				// set CustomerOfferAddresses
				Set<CustomerOfferAddress> coaddresses = new HashSet<CustomerOfferAddress>();
				coaddresses.add(new CustomerOfferAddress(t.getCity(), latitude, longitude, o));
				o.setCustomerOfferAddress(coaddresses);
				
				System.out.println(String.format("offerId: %d, truaxisId: %d, customerId: %d, status: %s, lat: %f, long: %f", 
						o.getId(), o.getTruAxisOfferId(), o.getCustomerId(), o.getStatus(), latitude, longitude));
			}
			
			// now attach matching offers back to customers
 			Set<CustomerOffer> set = new HashSet<CustomerOffer>(offerRepo.data.select((m) -> m.getCustomerId().equals(c.getId())));
			c.setCustomerOffers(set);
		}
		
		// set remaining truAxisOffer redemption type to new and report details
		for(TruAxisOffer t : truAxisRepo.findAll()){
			
			if(t.getRedemptionType() == null){ // non-associated truAxisOffer
				
				Random r = new Random();
				Double latitude = r.nextDouble() * 9 + 30.00;
				Double longitude = r.nextDouble() * 8 - 78.00;

				t.setRedemptionType("CASH");
				TruAxisOfferAddress addr = new TruAxisOfferAddress("Main St", latitude, longitude);
				List<TruAxisOfferAddress> addresses = new ArrayList<TruAxisOfferAddress>();
				addresses.add(addr);
				t.setAddresses(addresses);
				
				System.out.println(String.format("Unassociated truAxisOffer: %d, redemptionType: %s, EventStart: %s, EventEnd: %s, BookingStart: %s, BookingEnd: %s, lat: %f, long: %f", 
						t.getId(), t.getRedemptionType(), 
						dt.format(t.getEventStart()), dt.format(t.getEventEnd()), 
						dt.format(t.getBookingStart()), dt.format(t.getBookingEnd()),
						addr.getLatitude(), addr.getLongitude()));
			}else{
				System.out.println(String.format("truAxisOffer: %d, redemptionType: %s, EventStart: %s, EventEnd: %s, BookingStart: %s, BookingEnd: %s", 
					t.getId(), t.getRedemptionType(), 
					dt.format(t.getEventStart()), dt.format(t.getEventEnd()),
					dt.format(t.getBookingStart()), dt.format(t.getBookingEnd())));
			}
		}
		
		// add the dependency mappings
		map.put(CustomerRepository.class, customerRepo);
		map.put(CustomerOfferRepository.class, offerRepo);
		map.put(TruAxisOfferRepository.class, truAxisRepo);
		map.put(TruAxisService.class, new MockTruAxisService());
	}
	
	public static <T> T get(Class<T> type){
		return map.containsKey(type) ? (T) map.get(type) : null;
	}
	
	public static <T> void set(Class<T> type, T instance){
		if(map.containsKey(type)){
			map.remove(type);
		}
		
		map.put(type, instance);
	}
}
