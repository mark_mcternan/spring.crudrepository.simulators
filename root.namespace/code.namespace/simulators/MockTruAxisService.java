package com.datus.simulators;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.datus.domain.*;
import com.datus.service.TruAxisOfferRepository;
import com.datus.service.TruAxisService;
import com.datus.util.Constant;
import com.datus.util.DateUtil;

public class MockTruAxisService implements TruAxisService {
	
	@Override
	public TruAxisActivateOutput activateOffer(CustomerOffer offer) throws Exception {
		
		TruAxisOffer tao = offer.getTruAxisOffer();
		
		// kick out previously activated or redeemed
		if(offer.getStatus().compareTo(Constant.ACTIVATED) == 0 || 
		   offer.getStatus().compareTo(Constant.REDEEMED) == 0 ||
		   offer.getStatus().compareTo(Constant.EXPIRED) == 0)
		{
			throw new Exception(String.format("Offer is already {} and cannot be activated", offer.getStatus()));
		}
		
		// kick out data boundary issues
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh.mm.ss");
		Date today = new Date();
		
		// kick out if timestamp is before booking start timestamp
		if (today.before(tao.getBookingStart())){
			throw new Exception(String.format("Cannot activate offer before booking date of {}", tao.getBookingStart()));
		}
		
		// kick out if today (midnight) is after booking end date
		if(DateUtil.date(today).after(tao.getBookingEnd())){
			throw new Exception(String.format("Cannot activate offer after end date of {}", tao.getBookingEnd()));
		}
		
		// set good response
		TruAxisStatus status = new TruAxisStatus();
		status.setCode(600);
		
		TruAxisActivateOutputData data = new TruAxisActivateOutputData();
		data.setStatus(Constant.ACTIVATED);
		
		TruAxisActivateOutput response = new TruAxisActivateOutput();
		response.setStatus(status);
		response.setActivateData(data);
		
		return response;
	}

	@Override
	public TruAxisOfferDetail createOffer(TruAxisGetMatchedOffers matchedOffers, TruAxisOfferDatum offerDetails) throws Exception {
		// TODO Auto-generated method stub
		TruAxisOfferDetail offerDetail = new TruAxisOfferDetail();
		offerDetail.setOfferData(offerDetails);
		return offerDetail;
	}

	@Override
	public TruAxisMatchedOfferResponse getMatchedOffers(TruAxisGetMatchedOffers criteria) throws Exception {
		
		String userToken = criteria.getUserToken();
		Double userLongitude = criteria.getLongitude();
		Double userLatitude = criteria.getLatitude();
		Double latitude = userLatitude;
		
		int radius = criteria.getRadius();

		List<TruAxisOfferDatum> offerDatums = new ArrayList<TruAxisOfferDatum>();
		TruAxisMatchedOfferResponse offerResponse = new TruAxisMatchedOfferResponse();
		offerResponse.setTotalCount(10);
		
		// get current truAxisRepo
		MockTruAxisOfferRepository truRepo = (MockTruAxisOfferRepository) TestDependencyResolver.get(TruAxisOfferRepository.class);

		for(TruAxisOffer t : truRepo.data.select((m) -> m.getId() > 45, 10)){
			Double longitude = userLongitude + ((t.getId() - 45) / 100.0);
			t.setRedemptionType("CASH");
			
			List<TruAxisAddress> addresses = new ArrayList<TruAxisAddress>();
			TruAxisAddress address = new TruAxisAddress();
			address.setStreet(t.getCity());
			address.setLangitude(longitude);
			address.setLatitude(latitude);
			
			addresses.add(address);
			
			TruAxisOfferDatum offerDatum = new TruAxisOfferDatum();
			offerDatum.setOfferType(t.getOfferType());
			offerDatum.setOfferId(t.getOfferId());
			offerDatum.setBookingStartDate(t.getBookingStart());
			offerDatum.setBookingEndDate(t.getBookingEnd());
			offerDatum.setDisplayStartDate(t.getDisplayStart());
			offerDatum.setDisplayEndDate(t.getDisplayEnd());
			offerDatum.setEventStartDate(t.getEventStart());
			offerDatum.setEventEndDate(t.getEventEnd());
			offerDatum.setHeadline(t.getHeadline());
			offerDatum.setShortDescription(t.getShortDescription());
			offerDatum.setCity(t.getCity());
			offerDatum.setPrice(t.getPrice());
			offerDatum.setState(t.getState());
			offerDatum.setRegion(t.getRegion());
			offerDatum.setDiscountPercentage(t.getDiscountPercentage());
			offerDatum.setProductName(t.getProductName());
			offerDatum.setRedemptionType(t.getRedemptionType());
			offerDatum.setStatementCreditType(t.getStatementCreditType());
			offerDatum.setDetailedTerms(t.getDetailedTerms());
			offerDatum.setTerms(t.getTerms());
			
			
			TruAxisMerchant merchant = new TruAxisMerchant();
			merchant.setName(t.getMerchantName());
			merchant.setCategory(t.getMerchantCategory());
			merchant.setWebsiteUrl(t.getMerchantUrl());
			merchant.setStoreLocatorUrl(t.getStoreLocatorUrl());
			merchant.setAddresses(addresses);
			
			offerDatum.setMerchant(merchant);
			
			TruAxisOfferMedia offerMedia = new TruAxisOfferMedia();
			
			offerDatum.setOfferMedia(offerMedia);
			offerDatums.add(offerDatum);
		}
		
		offerResponse.setOfferData(offerDatums);
		return offerResponse;
	}

	@Override
	public TruAxisUserFeedbackOutput likeOffer(TruAxisUserFeedback feedback) throws Exception {
	
		// don't know of any rules if we got this far, so just return valid code
		TruAxisUserFeedbackOutput response = new TruAxisUserFeedbackOutput();
		TruAxisStatus status = new TruAxisStatus();
		status.setCode(600);
		response.setStatus(status);
		
		return response;
	}
}
