package com.datus.simulators;

import java.lang.reflect.Field;
import java.util.Random;

import com.datus.util.StringUtil;

/**
 * A class for generating a random value for a given field based on a user specified pattern
 * @author mark.mcternan
 */
public class FieldValueGenerator {
	
	private enum CharacterClasses{
		Digits,
		AlphaLower,
		AlphaUpper,
		AlphaNumeric;
	}
	
	/**
	 * used for generating sequences across all in-process instances of FieldValueGenerator
	 */
	private static long sequence = 0;
	
	/**
	 * instance vars
	 */
	private final String fieldName;
	private final String pattern;
	
	/**
	 * Initializes a new instance of a Field Value Generator class.
	 * @param fieldName Name of the field to be updated/generated.
	 * @param pattern Formatted string pattern comprised of literals and character class expressions which indicate the replacement type.
	 * <p>
	 * This class supports a small number of replacement expressions using standard characters which indicate the desired character 
	 * substitution type.  Standard characters were chosen over regex character class expressions so as to avoid forcing the user 
	 * to create tedious double-backslash escaped expressions, so that a YYYY-MM-DD HH:mm:SS mmm timestamp can be represented as:<br/>
	 * '20##-0#-0# 0#:0#:0# ###'<br/>
	 * instead of:<br/>
	 *  '\\d\\d\\d\\d-0\\d-0\\d 0\\d:0\\d:0\\d \\d\\d\\d'
	 * <p>
	 * Users can exclude portions of the pattern from substitution via a backslash escaped '\b' [ASCII backspace] boundary character
	 * that immediately precedes and follows the portion to be ignored. This is so the user can specify literal substrings which include
	 * replacement expression characters that would ordinarily be replaced.  The backspace character was chosen because it is exceedingly 
	 * unlikely to be required in a generated pattern, and therefore it is not supported in a pattern except as a boundary indicator.
	 * <p>
	 * Supported expressions include:
	 * <ul>
	 * 	<li> '#' - Numeric. replaced with a random digit [0-9].
	 *  <li> '*' - Alphabetic. replaced with a random, lower-case ASCII character [a-z]
	 *  <li> '^' - Uppercase. replaced with a random, upper-case ASCII character [A-Z]
	 *  <li> '%' - Complete set. replaced with a random ASCII AlphaNumeric character or digit [a-zA-Z0-9].
	 *  <li> '\b' - Escape Boundary.  Bypasses any substring between this and the next instance of '\b'. 
	 *  This permits the user to specify a literal '#', '*', '^', '%' that will not be replaced, for example: 
	 *  '\bWe are #1!\b' or 'We are \b#\b1!' will both produce the literal string 'We are #1!'
	 * </ul>
	 * 
	 * FieldValueGenerators also support a common requirement for generating fields which consist of sequential long values.
	 * If a field's pattern is set to null, the named field will automatically be set to the next long value in the sequence.
	 * This sequence is shared across all FieldValueGenerators in the current process, so it is not thread safe.
	 * If the pattern is set to a zero length string than the field will be assigned the same value as the previously incremented sequence:
	 * e.g: given an internal value of 234, an empty string pattern will yield 234 and a null pattern will yield 235.
	 */
	public FieldValueGenerator(String fieldName, String pattern){
		this.fieldName = fieldName;
		this.pattern = pattern;
	}
	
	/**  Accessors */
	
	/**
	 * Name of Property to generate
	 * @return Property name as String
	 */
	public String getFieldName(){
		return this.fieldName;
	}
	
	/**
	 * Pattern
	 * @return Pattern as String
	 */
	public String getPattern(){
		return this.pattern;
	}
	
	/**
	 * Updates a Target instance's field with a randomly generated value based on a user supplied pattern
	 * Target fields should be private, protected or public instance variables.  Attempting to set a final
	 * or static variable will result in an Illegal Access Exception.
	 * @param type Specifies the target field's datatype
	 * @param target The object to update
	 */
	public <T> void generate(Class<T> type, T target) {
		
		try{		
			// get specified field
			Field field = type.getDeclaredField(this.fieldName);
			field.setAccessible(true);
			
			// test for sequence field
			if(this.pattern == null){
				String seq = new Long(++FieldValueGenerator.sequence).toString();
				field.set(target, StringUtil.convertType(field.getType(), seq));
				return;
			}
			
			// test for repeated sequence field
			if(this.pattern.isEmpty()){
				String seq = new Long(FieldValueGenerator.sequence).toString();
				field.set(target, StringUtil.convertType(field.getType(), seq));
				return;
			}
			
			// update Target's field with appropriately converted value from pattern
			field.set(target, FieldValueGenerator.generateValue(field.getType(), this.pattern));
			return;
			
		}catch(NoSuchFieldException nsf){
			// rethrow because this shouldn't be permitted
			nsf.printStackTrace();
		} catch (IllegalArgumentException e){
			e.printStackTrace();
		}catch(IllegalAccessException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Generates a value based on a user supplied pattern
	 * @param type Specifies the return datatype
	 * @param pattern The pattern to generate
	 */
	public static <T> T generateValue(Class<T> type, String pattern) {
		
		try{
			
			// replace char class expressions
			String[] buffer = pattern.split("\b");
			
			for(int i = 0; i < buffer.length; i += 2){ // ignore even substrings as they are escaped.
				String substr = buffer[i];
				
				while(substr.contains("#"))
				{
					substr = substr.replaceFirst("#", FieldValueGenerator.generateChar(CharacterClasses.Digits));
				}
				
				while(substr.contains("^"))
				{
					substr = substr.replaceFirst("\\^", FieldValueGenerator.generateChar(CharacterClasses.AlphaUpper));
				}
				
				while(substr.contains("*"))
				{
					substr = substr.replaceFirst("\\*", FieldValueGenerator.generateChar(CharacterClasses.AlphaLower));
				}
				
				while(substr.contains("%"))
				{
					substr = substr.replaceFirst("%", FieldValueGenerator.generateChar(CharacterClasses.AlphaNumeric));
				}
				
				// replace element with generated content
				buffer[i] = substr;
			}
			
			String output = StringUtil.join(buffer, "");
			
			return StringUtils.convertType(type, output);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Generate a random character for a given character class
	 * @param code Character Class to generate
	 * @return a char in String form corresponding to the specified class
	 */
	private static String generateChar(CharacterClasses code){
		
		int boundary = -1;
		Random r = new Random();
		
		// AlphaNumeric subclass filter
		if(code == CharacterClasses.AlphaNumeric){
			boundary = r.nextInt(62);
			if(boundary < 10){ 			// 0-9: digits;
				code = CharacterClasses.Digits;
			}else if(boundary < 36){ 	// 10-35: Upper;
				code = CharacterClasses.AlphaUpper;
				boundary -= 10;
			}else{						// 36-61: Lower
				code = CharacterClasses.AlphaLower;
				boundary -= 36;
			}
		}
		
		switch(code){
			case Digits:
				boundary = (boundary != -1) ? boundary + '0': r.nextInt(10) + '0';
				break;
			case AlphaLower:
				boundary = (boundary != -1) ? boundary + 'a': r.nextInt(26) + 'a';
				break;
			case AlphaUpper:
				boundary = (boundary != -1) ? boundary + 'A': r.nextInt(26) + 'A';
				break;
		}

		return Character.toString((char) boundary);
	}
}
