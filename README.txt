Implementations of generically typed MockRepositories that implement the Java Spring's
org.springframework.data.repository.CrudRepository<T, ID> Interface.  

Requires Java 1.8 for lambda support.

The intention of this code is to be the basis of customized classes which can be injected 
into production code for demos, load testing, or integration testing where Mocking is not 
practical.  Customization will undoubtedly be required, but typical database/querying 
capabilities are implemented so that you can mock any datasource and wrap it in a Mock 
Datasource that implements the Interface you require.  The implementation of the 
CrudRepository is just one possible manifestation of this simulator code.  Unit testing 
code is just provided for illustrative purposes, as this code is really intended to be 
used for scenarios in which injected dependencies either don't exist or are otherwise 
unavailable. 

Several repos which were implemented from this MockRepository are included as examples.  
These repos were actually used during a real project to permit development and testing 
of production code that would have been blocked if we had had to wait for the real 
dependencies to become available.

From the javadoc of "TestDataContainer":
 * TestDataContainers are state machines used to load, manipulate and maintain state inside of a 'simulator'.
 * Unlike traditional mocks, which produce predetermined behavior for an isolated dependency, 
 * a simulator is a stand-in for a sub-set of interrelated functionality provided by an external 
 * subsystem which is either unavailable or impractical to execute against during a 
 * demo, load test, or advanced testing scenario.
 * 
 * To accomplish this end, simulators must maintain consistent (i.e., largely deterministic) 
 * state and behavior.  A combination of ordered record set & cursor, a TestDataContainer is intended 
 * to encapsulate this data and expose functionality for typical manipulations of this data, such as:
 * <ul>
 *  <li>return an item count</li>
 *  <li>return a copy of the entire set</li>
 *  <li>return a subset based on some user supplied criteria.</li>
 *  <li>traverse an indexed set by moving forwards or backwards.</li>
 *  <li>insert an item</li>
 *  <li>update any items that match some criteria.</li>
 *  <li>remove an items that match some criteria</li>
 * </ul>

- code.namespace.simulators contains:
	+ implementations and interface of TestDataContainer  (Read the javadoc commentary to understand 
    the intention of a TDC.
	+ Specializations of MockRepository Base class that implements the 
    org.springframework.data.repository.CrudRepository (A very common repo)
	+ FieldValueGenerator is a generic value generator used to populate fields in domain objects of 
    T which are then loaded into a TestDataContainer.
	
- usage examples of simulators in "test.namespace"
- code.namespace.util contains some add on functions that are used within the simulators.