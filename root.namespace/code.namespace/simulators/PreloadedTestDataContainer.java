package com.datus.simulators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.function.BiFunction;
import java.util.function.Predicate;

/**
 * @author mbm
 * Base implementation of the TestDataContainer
 */
public class PreloadedTestDataContainer<T> implements TestDataContainer<T> {
	
	private final Class<T> type;
	private final List<T> data;
	private ListIterator<T> iterator;

	/** constructor
	 * 
	 * @param type
	 * @param data List of type T to save
	 */
	public PreloadedTestDataContainer(Class<T> type, List<T> data){
		this.type = type;
		this.data = data;
		this.iterator = this.data.listIterator();
	}
	
	@Override
	public int getCount() {
		return (this.data.size() == Integer.MAX_VALUE) ? -1 : this.data.size();
	}
	
	@Override
	public Class<T> getType() {
		return this.type;
	}

	@Override
	public void clear() {
		this.data.clear();
		this.resetIterator(0);
	}

	@Override
	public T insert(T item) {
		
		if(!this.data.add(item)){
			return null;
		}
		// update iterator and return
		this.resetIterator();
		return item;
	}

	@Override
	public T next() {
		return (this.iterator.hasNext()) ? this.iterator.next() : null;
	}

	@Override
	public List<T> next(int count) {
		List<T> sublist = new ArrayList<T>();
		while(this.iterator.hasNext() && count > 0){
			sublist.add(this.iterator.next());
			count--;
		}
		
		return sublist;
	}
	
	@Override
	public int remove(Predicate<T> matchTest) {
		return this.remove(matchTest, -1);
	}

	@Override
	public int remove(Predicate<T> matchTest, int limit) {
		Collection<T> matches = this.select(matchTest, limit);
		this.data.removeAll(matches);
		return matches.size();
	}
	
	@Override
	public Collection<T> select(Predicate<T> matchTest){
		return this.select(matchTest, -1);
	}
	
	@Override
	public Collection<T> select(Predicate<T> matchTest, int limit){
		
		List<T> matches = new ArrayList<T>();
		
		for(T item : this.data){
			
			if(matchTest.test(item) && limit-- != 0){
				matches.add(item);
			}
			
			// exit if user supplied limit of '0', or specified match limit has been reached
			if(limit == 0){ 
				break;
			}
		}
		
		return matches;
	}

	@Override
	public List<T> selectAll() {
		
		// create new array of current
		T[] copy = (T[]) this.data.toArray();

		// returns partial implementation of List w no add() remove() support.
		return Arrays.asList(copy);
	}
	
	@Override
	public <V> Collection<T> update(Predicate<T> matchTest, BiFunction<Predicate<T>, V, Collection<T>> transformer) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * updates the internal list iterator to match the current data and repositions it to match
	 * it's predecessors last position if possible, or to the beginning of the set if not.
	 * 
	 * @return whether restoring the iterator to it's predecessor's position succeeded
	 */
	private boolean resetIterator(){
		return this.resetIterator(this.iterator.nextIndex());
	}
	
	/**
	 * updates the internal list iterator to match the current data and repositions it to match
	 * it's predecessors last position if possible, or to the beginning of the set if not.
	 * 
	 * @param offset number of elements to shift.  
	 * This permits forcing the reset to a particular position, as in the case of a clear().
	 * 
	 * @return whether restoring the iterator to it's predecessor's position succeeded
	 */
	private boolean resetIterator(int offset)
	{
		// determine if relocation is possible
		boolean possible = (offset == 0 || offset < this.data.size());
		offset = possible ? offset : 0;
		
		// reset the iterator and reposition
		this.iterator = this.data.listIterator();
		while(this.iterator.hasNext() && this.iterator.nextIndex() < offset){
			this.iterator.next();
		}
		return possible;
	}
}
