package com.datus.test.helpers;

/**
 * To support J-Unit test grouping
 */
public interface IntegrationTest extends TestCategory {}
