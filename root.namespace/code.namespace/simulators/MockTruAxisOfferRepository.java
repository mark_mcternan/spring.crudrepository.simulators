package com.datus.simulators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.datus.domain.TruAxisOffer;
import com.datus.service.TruAxisOfferRepository;

public class MockTruAxisOfferRepository extends MockRepository<TruAxisOffer, Long> implements TruAxisOfferRepository
{
	/**
	 * Initializes a new instance of a MockTruAxisOfferRepository
	 * @param testData A test data container containing state for the Repo.
	 */
	public MockTruAxisOfferRepository(TestDataContainer<TruAxisOffer> testData)
	{
		super(testData);
	}
	
	@Override
	public void delete(Long id) {
		this.data.remove((m) -> m.getId().equals(id));
	}

	@Override
	public void delete(TruAxisOffer offer) {
		this.delete(offer.getId());
	}
	
	@Override
	protected Collection<TruAxisOffer> findAllMatchesInKeySet(Set<Long> keyset){
		return this.data.select((m) -> keyset.contains(m.getId()));
	}
	
	@Override
	public TruAxisOffer findOne(Long id) {
		List<TruAxisOffer> set = new ArrayList<TruAxisOffer>(this.data.select((m) -> m.getId().equals(id)));
		return set.size() > 0 ? set.get(0) : null;
	}

	@Override
	public <S extends TruAxisOffer> S save(S entity) {
		
		List<TruAxisOffer> matches = new ArrayList<TruAxisOffer>(
			this.data.select((m) -> m.getId().equals(entity.getId())));
		
		// update all matching items with new values
		for(TruAxisOffer tao : matches){
			this.copyFieldValues(entity, tao);
		}
		
		// return first saved item
		return matches.size() > 0 ? (S) matches.get(0) : null;
	}

	@Override
	public TruAxisOffer findByOfferId(String offerId) {
		List<TruAxisOffer> match = new ArrayList<TruAxisOffer>(this.data.select((m) -> m.getOfferId().compareTo(offerId) == 0, 1));
		return match.size() > 0 ? match.get(0) : null;
	}
	
	/**
	 * A helper method to generate a TestDataContainer of TruAxisOffers using default Field Value Generators
	 * @param quantityToGenerate number of TruAxisOffers to create
	 * @return a populated TestDataContainer
	 */
	public static TestDataContainer<TruAxisOffer> generateTruAxisOffers(long quantityToGenerate){
		
		// default field configuration
		FieldValueGenerator[] generators = {
			new FieldValueGenerator("id", null),
			new FieldValueGenerator("offerId", ""),
			new FieldValueGenerator("offerType", "^****"),
			new FieldValueGenerator("headline", "^***** ^*****"),
			new FieldValueGenerator("shortDescription", "^****** **** *** *** ***"),
			new FieldValueGenerator("displayStart", "2015-02-0#"),
			new FieldValueGenerator("displayEnd", "2015-02-1#"),
			new FieldValueGenerator("eventStart", "2015-02-0#"),
			new FieldValueGenerator("eventEnd", "2015-02-1#"),
			new FieldValueGenerator("bookingStart", "2015-02-0#"),
			new FieldValueGenerator("bookingEnd", "2015-02-1#"),
			new FieldValueGenerator("city", "^****"),
			new FieldValueGenerator("state", "^^"),
			new FieldValueGenerator("region", "^******"),
			new FieldValueGenerator("productName", "^******"),
			new FieldValueGenerator("price", "##.##"),
			new FieldValueGenerator("merchantName", "^********"),
			new FieldValueGenerator("merchantCategory", "Online"),
			new FieldValueGenerator("imageURL", "http://www.contoso.com"),
			new FieldValueGenerator("merchantUrl", "http://www.contoso.com"),
			new FieldValueGenerator("merchantImageUrl1", "http://www.contoso.com"),
			new FieldValueGenerator("storeLocatorUrl", "http://www.contoso.com")
		};
		
		return MockTruAxisOfferRepository.generateTruAxisOffers(quantityToGenerate, generators);
	}
	
	/**
	 * A helper method to generate a TestDataContainer of TruAxisOffers using user supplied Field Value Generators
	 * @param quantityToGenerate Number of TruAxisOffers to create
	 * @param generators User supplied set of FieldValueGenerators for supplying formatted field values
	 * @return A populated TestDataContainer of TruAxisOffers
	 */
	public static TestDataContainer<TruAxisOffer> generateTruAxisOffers(long quantityToGenerate, FieldValueGenerator[] generators){
		return new GeneratedTestDataContainer<TruAxisOffer>(TruAxisOffer.class, generators, quantityToGenerate);
	}
}
