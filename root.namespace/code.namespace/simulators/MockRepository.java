package com.datus.simulators;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.springframework.data.repository.CrudRepository;
//import org.jmock.Mockery;

/**
 * Base class containing common functionality for MockRepositories.
 * Members which are marked as abstract depend upon type specific 
 * functionality that cannot be contained in the common base class
 * 
 * @author mark.mcternan
 *
 * @param <T> The Class upon which the MockRepository operates
 * @param <ID> The Data Type of the field which uniquely identifies a T (typically Long)
 */
public abstract class MockRepository<T, ID  extends Serializable> implements CrudRepository<T, ID>
{
	/**
	 *  instance vars
	 */
	protected final TestDataContainer<T> data;
	
	/**
	 * constructor
	 * @param testData TestContainer of Type T containing data to manipulate
	 * @param type     Class of Type T
	 */
	public MockRepository(TestDataContainer<T> testData)
	{
		this.data = testData;
	}
	
//	/**
//	 * Inner Mockery Context
//	 * @return
//	 */
//	protected Mockery getContext(){
//		return this.context;
//	}
	
	/**
	 * Provides direct access to internal data for testing purposes
	 * @return The Repository's internal TestDataContainer
	 */
	public TestDataContainer<T> getData(){
		return this.data;
	}
	
	@Override
	public long count(){
		return this.data.getCount();
	}
	
	@Override
	public abstract void delete(ID id);
	
	@Override
	public abstract void delete(T entity);
	
	@Override
	public void delete(Iterable<? extends T> entities) {
		for(T e : entities){
			this.delete(e);
		}
	}

	@Override
	public void deleteAll() {
		this.data.clear();
	}
	
	@Override
	public boolean exists(ID id){
		return this.findOne(id) != null;
	}
	
	@Override
	public Iterable<T> findAll(){	
		return this.data.selectAll();
	}
	
	@Override
	public Iterable<T> findAll(Iterable<ID> ids){
		
		Set<ID> keys = new HashSet<ID>();
		for(ID id : ids){
			if(!keys.contains(id)){ keys.add(id); }
		}
		
		return this.findAllMatchesInKeySet(keys);
	}
	
	@Override
	public abstract T findOne(ID id);
	
	@Override
	public abstract <S extends T> S save(S entity);

	@Override
	public <S extends T> Iterable<S> save(Iterable<S> entities) {
		
		for(S e : entities){
			this.save(e);
		}
		
		return entities;
	}
	
	/**
	 * Copies the declared field values of one object to another
	 * @param source The object to copy from
	 * @param target The object to copy to
	 * @return the updated object
	 */
	protected void copyFieldValues(T source, T target){
		
		Field[] flds = source.getClass().getDeclaredFields();
			
		for(Field fld : flds){
			// screen out any final modified fields
			if((fld.getModifiers() & Modifier.FINAL) != 0){
				continue;
			}
			
			try {
				fld.setAccessible(true);
				fld.set(target, fld.get(source));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
	
	protected abstract Collection<T> findAllMatchesInKeySet(Set<ID> keyset);
}
