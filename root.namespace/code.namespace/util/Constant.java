package com.datus.util;

public final class Constant {
	
	// CustomerOffer Status
	public static final String NEW = "NEW";
	public static final String SENT = "SENT";
	public static final String ACTIVATED = "ACTIVATED";
	public static final String EXPIRED = "EXPIRED";
	public static final String REDEEMED = "REDEEMED";
	
	// Notification Types
	public static final String NEW_OFFERS = "newOffers";
	public static final String REDEEMED_OFFER = "redeemedOffer";
	
	private Constant() {}
}
