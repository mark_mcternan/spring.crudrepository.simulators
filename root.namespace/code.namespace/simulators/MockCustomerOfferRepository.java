package com.datus.simulators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.datus.domain.CustomerOffer;
import com.datus.domain.TruAxisOffer;
import com.datus.service.CustomerOfferRepository;
import com.datus.util.DateUtil;

public class MockCustomerOfferRepository extends MockRepository<CustomerOffer, Long> implements CustomerOfferRepository {

	/**
	 * Initializes a new instance of a MockCustomerOfferRepository
	 * @param testData A test data container containing state for the Repo.
	 */
	public MockCustomerOfferRepository(TestDataContainer<CustomerOffer> testData) {
		super(testData);
	}
	
	@Override
	public void delete(Long id) {
		this.data.remove((m) -> m.getId().equals(id));
	}

	@Override
	public void delete(CustomerOffer offer) {
		this.delete(offer.getId());
	}
	
	@Override
	protected Collection<CustomerOffer> findAllMatchesInKeySet(Set<Long> keyset){
		return this.data.select((m) -> keyset.contains(m.getId()));
	}
	
	@Override
	public CustomerOffer findOne(Long id) {
		List<CustomerOffer> set = new ArrayList<CustomerOffer>(this.data.select((m) -> m.getId().equals(id), 1));
		return set.size() > 0 ? set.get(0) : null;
	}

	@Override
	public <S extends CustomerOffer> S save(S entity) {
		List<CustomerOffer> matches = new ArrayList<CustomerOffer>(
			this.data.select((m) -> m.getId().equals(entity.getId())));
			
		// update all matching items with new values
		for(CustomerOffer o : matches){
			this.copyFieldValues(entity, o);
		}
			
		// return first saved item
		return matches.size() > 0 ? (S) matches.get(0) : null;
	}
	
	@Override
	public List<CustomerOffer> findByDateRedeemedBetween(Long customerId, Date startDate, Date endDate) {
		
		List<CustomerOffer> output = new ArrayList<CustomerOffer>();
		Date start = DateUtil.date(startDate);
		Date end = DateUtil.date(endDate);
		
		output.addAll(this.data.select((o) -> {
			Date redeemed = DateUtil.date(o.getDateRedeemed());
			return o.getCustomerId().equals(customerId) && 
				   !redeemed.before(start) && 
				   !redeemed.after(end);
		}));
		
		return output;
	}

	@Override
	public List<CustomerOffer> findByDateSubscribed(Long customerId, Date dateSubscribed) {
 
		List<CustomerOffer> output = new ArrayList<CustomerOffer>();
		Date subDate = DateUtil.date(dateSubscribed);
		
		output.addAll(this.data.select((o) -> {
			return (o.getCustomerId().equals(customerId) && 
					subDate.equals(DateUtil.date(o.getDateSubscribed())));
		}));
		
		return output;
	}

	@Override
	public List<CustomerOffer> findByStatus(Long customerId, String status) {
		
		List<CustomerOffer> output = new ArrayList<CustomerOffer>();
		output.addAll(this.data.select((o) -> {
			return (o.getCustomerId().equals(customerId) && 
					o.getStatus().compareTo(status) == 0);
		}));
		
		return output;
	}

	@Override
	public List<CustomerOffer> findOffersSubscribedToOnOrLaterThan(Long customerId, Date date) {
		
		List<CustomerOffer> output = new ArrayList<CustomerOffer>();
		Date subDate = DateUtil.date(date);
		
		output.addAll(this.data.select((o) -> {
			return o.getCustomerId().equals(customerId) && 
				   subDate.compareTo(DateUtil.date(o.getDateSubscribed())) <= 0;
		}));
		
		return output;
	}

	/**
	 * WARNING:  This method requires test CustomerOffers to have populated TruAxisOffer properties
	 */
	@Override
	public List<CustomerOffer> findOffersExpiringInRange(Long customerId, Date startDate, Date endDate) {
		
		List<CustomerOffer> output = new ArrayList<CustomerOffer>();
		Date start = DateUtil.date(startDate);
		Date end = DateUtil.date(endDate);
		
		try{
			output.addAll(this.data.select((o) -> {
				TruAxisOffer tao = o.getTruAxisOffer();
				Date expires = DateUtil.date(tao.getEventEnd());
				return o.getCustomerId().equals(customerId) && 
					   !expires.before(start) && 
					   !expires.after(end);
			}));
			
			return output;
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public CustomerOffer findByCustomerIdAndTruAxisOfferId(Long customerId, Long truAxisOfferId) {
		
		List<CustomerOffer> matches = new ArrayList<CustomerOffer>(
			this.data.select((m) -> m.getCustomerId().equals(customerId) && 
									m.getTruAxisOfferId().equals(truAxisOfferId), 1));
		
		return matches.size() > 0 ? matches.get(0) : null;
	}
	
	/**
	 * A helper method to generate a TestDataContainer of CustomerOffers using default Field Value Generators
	 * @param quantityToGenerate Number of CustomerOffers to create
	 * @return A populated TestDataContainer
	 */
	public static TestDataContainer<CustomerOffer> generateCustomerOffers(long quantityToGenerate){
		
		// default field configuration
		FieldValueGenerator[] generators = {
			new FieldValueGenerator("id", null),
			new FieldValueGenerator("customerId", ""),
			new FieldValueGenerator("truAxisOfferId", ""),
			new FieldValueGenerator("dateSubscribed", "2015-0#-1#"),
			new FieldValueGenerator("dateRedeemed", "2015-0#-1#"),
			new FieldValueGenerator("status", "^***"),
			new FieldValueGenerator("liked", "false")
		};
		
		return MockCustomerOfferRepository.generateCustomerOffers(quantityToGenerate, generators);
	}
	
	/**
	 * A helper method to generate a TestDataContainer of CustomerOffers using user supplied Field Value Generators
	 * @param quantityToGenerate Number of CustomerOffers to create
	 * @param generators User supplied set of FieldValueGenerators for supplying formatted field values
	 * @return A populated TestDataContainer of CustomerOffers
	 */
	public static TestDataContainer<CustomerOffer> generateCustomerOffers(long quantityToGenerate, FieldValueGenerator[] generators){
		return new GeneratedTestDataContainer<CustomerOffer>(CustomerOffer.class, generators, quantityToGenerate);
	}
}
