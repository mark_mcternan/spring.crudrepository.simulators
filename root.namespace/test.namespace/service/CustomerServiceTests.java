package com.datus.test.service;

// JUnit support
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import junit.framework.TestCase;

import org.jmock.Mockery;
import org.jmock.Expectations;

// springframework autowired DI support
import org.springframework.test.util.ReflectionTestUtils;

import com.datus.domain.Customer;
import com.datus.domain.CustomerOffer;
import com.datus.service.*;
import com.datus.simulators.MockCustomerOfferRepository;
import com.datus.simulators.MockCustomerRepository;
import com.datus.simulators.PreloadedTestDataContainer;
import com.datus.simulators.TestDataContainer;
import com.datus.util.DateUtil;

public class CustomerServiceTests extends TestCase {

	@Test
	public final void testGetCustomer_WithInvalidToken_ReturnsNull() {
		
		// Arrange - using J-Mock
		Mockery context = new Mockery();
		final CustomerRepository repo = context.mock(CustomerRepository.class);
		context.checking(new Expectations() {{
			exactly(1).of(repo).findByToken(
					with(equal("badtoken")));
					will(returnValue(null));
		}});
		
		// set up the customer service and inject the repo
		CustomerService cs = new CustomerServiceImpl();
		ReflectionTestUtils.setField(cs, "customerRepository", repo);
		
		// act
		Customer actual = cs.getCustomer("badtoken");
		
		// assert
		context.assertIsSatisfied();
		//org.junit.Assert.assertNull("Returned non-null customer for invalid token", actual);
	}
	
	@Test
	public final void testGetCustomer_WithValidToken_ReturnsCustomer() {
		
		// Arrange - using J-Mock
		Mockery context = new Mockery();
		final CustomerRepository repo = context.mock(CustomerRepository.class);
		context.checking(new Expectations() {{
			exactly(1).of(repo).findByToken(
				with(equal("goodtoken")));
				will(returnValue(
						new Customer("token", "x", "foo", "bar", "mobile", "", "")
				));
		}});
			
		// set up the customer service and inject the repo
		CustomerService cs = new CustomerServiceImpl();
		ReflectionTestUtils.setField(cs, "customerRepository", repo);
				
		// Act
		Customer actual = cs.getCustomer("goodtoken");
				
		// assert
		context.assertIsSatisfied();
		org.junit.Assert.assertNotNull("Returned null customer for valid token", actual);
	}

	@Test
	public final void testGetCustomerByTruAxisToken_WithValidToken_ReturnsCustomer() {
		
		// Arrange - using Preloaded Data Container
		List<Customer> data = new ArrayList<Customer>();
		data.add(new Customer("token", "x", "foo", "bar", "mobile", "", ""));
		data.add(new Customer("token", "x", "foo", "bar", "mobile", "", ""));
		data.add(new Customer("token", "12345", "foo", "bar", "mobile", "", ""));
		
		TestDataContainer<Customer> tdc = new PreloadedTestDataContainer<Customer>(Customer.class, data);
		
		CustomerRepository repo = new MockCustomerRepository(tdc);
		CustomerService cs = new CustomerServiceImpl();
		ReflectionTestUtils.setField(cs, "customerRepository", repo);
		
		// Act
		Customer actual = cs.getCustomerByTruAxisToken("12345");
		
		// Assert
		org.junit.Assert.assertNotNull("Returned null customer for valid token", actual);
	}
	
	@Test
	public final void testGetCustomerByTruAxisToken_WithInvalidToken_ReturnsNull() {
		
		// Arrange - using Generated Data Container w default default customer generation
		CustomerRepository repo = new MockCustomerRepository(MockCustomerRepository.generateCustomers(5));
		CustomerService cs = new CustomerServiceImpl();
		ReflectionTestUtils.setField(cs, "customerRepository", repo);
		
		// Act
		Customer actual = cs.getCustomerByTruAxisToken("111"); // will fail because default tokens are generated w 6 chars.
		
		// Assert
		org.junit.Assert.assertNotNull("Returned null customer for valid token", actual);
	}

	@Test
	public final void testGetRedeemedCustomerOffers_WithValidCustomer_ReturnsOffersFromLast60Days() {
		
		// Arrange - using Generated Test Data Container w/ test data massaging	
		CustomerOfferRepository repo = new MockCustomerOfferRepository(MockCustomerOfferRepository.generateCustomerOffers(25));
		Customer c = new Customer();
		c.setId(new Long(4));
		
		// directly adjust generated offer data for test
		Calendar cal = DateUtil.today();
		cal.add(Calendar.DAY_OF_YEAR, -75); // set calendar 75 days back
		for(CustomerOffer offer : repo.findAll()){
			cal.add(Calendar.DAY_OF_YEAR, 1);
			offer.setDateRedeemed(cal.getTime()); // should yield 11 offers in last 60 days
			offer.setCustomerId(c.getId());
		}
		
		// inject the repo
		CustomerService cs = new CustomerServiceImpl();
		ReflectionTestUtils.setField(cs, "customerOfferRepository", repo);
		
		// Act
		List<CustomerOffer> offers = cs.getRedeemedCustomerOffers(c);
		
		// Assert
		org.junit.Assert.assertEquals("Failed to find 11 records", 11, offers.size());
	}

	@Test
	public final void testGetRedeemedCustomerOffers_WithValidCustomerAndDayCount_ReturnsOffersFromDayCount() {
		
		// Arrange - using Generated Test Data Container w/ test data massaging	
		CustomerOfferRepository repo = new MockCustomerOfferRepository(MockCustomerOfferRepository.generateCustomerOffers(25));
		Customer c = new Customer();
		c.setId(new Long(5));
				
		// directly adjust generated offer data for test
		Calendar cal = DateUtil.today();
		cal.add(Calendar.DAY_OF_YEAR, -25); // set calendar 75 days back
		for(CustomerOffer offer : repo.findAll()){
			cal.add(Calendar.DAY_OF_YEAR, 1);
			offer.setDateRedeemed(cal.getTime()); // should yield 11 offers in last 60 days
			offer.setCustomerId(c.getId());
		}
				
		// inject the repo
		CustomerService cs = new CustomerServiceImpl();
		ReflectionTestUtils.setField(cs, "customerOfferRepository", repo);
				
		// Act
		List<CustomerOffer> offers = cs.getRedeemedCustomerOffers(c, 7);
				
		// Assert
		org.junit.Assert.assertEquals("Failed to find 8 records", 8, offers.size());
	}

	@Test
	public final void testGetCustomerOffersByDate() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testGetCustomerOffer() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testLikeCustomerOffer() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testUpdateOffersForCustomerByLocation() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testUpdateCustomerOfferStatus() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testGetCustomerOffersByStatus() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testGetCustomerOffersAboutToExpire() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testGetRecentCustomerOffers() {
		fail("Not yet implemented"); // TODO
	}
}
