package com.datus.test.helpers;

/**
 * To support J-Unit test grouping
 */
public interface UnitTest extends TestCategory {}
