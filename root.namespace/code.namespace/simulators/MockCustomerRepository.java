package com.datus.simulators;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.datus.domain.Customer;
import com.datus.service.CustomerRepository;

public class MockCustomerRepository extends MockRepository<Customer, Long> implements CustomerRepository
{
	/**
	 * Initializes a new instance of the MockCustomerRepository
	 * @param testData A test data container containing state for the Repo.
	 */
	public MockCustomerRepository(TestDataContainer<Customer> testData)
	{
		super(testData);
	}
	
	@Override
	public void delete(Long id) {
		this.data.remove(m -> m.getId().equals(id));
	}

	@Override
	public void delete(Customer entity) {
		this.delete(entity.getId());
	}
	
	@Override
	protected Collection<Customer> findAllMatchesInKeySet(Set<Long> keyset){
		return this.data.select((m) -> keyset.contains(m.getId()));
	}
	
	@Override
	public Customer findOne(Long id){
		List<Customer> set = new ArrayList<Customer>(this.data.select((m) -> m.getId().equals(id)));
		return set.size() > 0 ? set.get(0) : null;
	}
	

	@Override
	public <S extends Customer> S save(S entity) {
		
		List<Customer> matches = new ArrayList<Customer>(this.data.select((m) -> m.getId().equals(entity.getId())));
		
		// update all matching items
		for(Customer c : matches){
			this.copyFieldValues(entity, c);
		}
		
		// return first saved item
		return matches.size() > 0 ? (S) matches.get(0) : null;
	}
	
	@Override
	public Customer findByTruAxisToken(String truAxisToken) {
		List<Customer> match = new ArrayList<Customer>(
			this.data.select((m) -> m.getTruAxisToken().compareTo(truAxisToken) == 0, 1));
		
		return match.size() > 0 ? match.get(0) : null;
	}

	@Override
	public Customer findByMobileNumber(String mobileNumber) {
		List<Customer> match = new ArrayList<Customer>(
			this.data.select((m) -> m.getMobileNumber().compareTo(mobileNumber) == 0, 1));
		
		return match.size() > 0 ? match.get(0) : null;
	}

	@Override
	public Customer findByToken(String token) {
		List<Customer> match = new ArrayList<Customer>(this.data.select((m)-> m.getToken().compareTo(token) == 0, 1));
		
		return match.size() > 0 ? match.get(0) : null;
	}
	
	/**
	 * A helper method to generate a TestDataContainer of customers using default Field Value Generators
	 * @param quantityToGenerate number of customers to create
	 * @return a populated TestDataContainer
	 */
	public static TestDataContainer<Customer> generateCustomers(long quantityToGenerate){
		
		// default field configuration
		FieldValueGenerator[] generators = {
				new FieldValueGenerator("id", null),
				new FieldValueGenerator("token", "%%%%-%%"),
				new FieldValueGenerator("truAxisToken", "%%%%%%"),
				new FieldValueGenerator("firstName", "^*******"),
				new FieldValueGenerator("lastName", "^***********"),
				new FieldValueGenerator("mobileNumber", "+1-(###) ###-####"),
				new FieldValueGenerator("latitude", "38.9#"),
				new FieldValueGenerator("longitude", "-77.##"),
				new FieldValueGenerator("lastLocationUpdate", new SimpleDateFormat("yyyy-MM-dd").format(new Date()))
		};
		
		return MockCustomerRepository.generateCustomers(quantityToGenerate, generators);
	}
	
	/**
	 * A helper method to generate a TestDataContainer of customers using user supplied Field Value Generators
	 * @param quantityToGenerate Number of customers to create
	 * @param generators User supplied set of FieldValueGenerators for supplying formatted field values
	 * @return A populated TestDataContainer of Customers
	 */
	public static TestDataContainer<Customer> generateCustomers(long quantityToGenerate, FieldValueGenerator[] generators){
		return new GeneratedTestDataContainer<Customer>(Customer.class, generators, quantityToGenerate);
	}
}
