/**
 * @author mbm
 */
package com.datus.simulators;

import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;

/**
 * <b>Remarks:</b>
 * <p>
 * TestDataContainers are state machines used to load, manipulate and maintain state inside of a 'simulator'.
 * Unlike traditional mocks, which produce predetermined behavior for an isolated dependency, 
 * a simulator is a stand-in for a sub-set of interrelated functionality provided by an external 
 * subsystem which is either unavailable or impractical to execute against during a 
 * demo, load test, or advanced testing scenario.
 * 
 * To accomplish this end, simulators must maintain consistent (i.e., largely deterministic) 
 * state and behavior.  A combination of ordered record set & cursor, a TestDataContainer is intended 
 * to encapsulate this data and expose functionality for typical manipulations of this data, such as:
 * <ul>
 * 	<li>return an item count</li>
 *  <li>return a copy of the entire set</li>
 *  <li>return a subset based on some user supplied criteria.</li>
 *  <li>traverse an indexed set by moving forwards or backwards.</li>
 *  <li>insert an item</li>
 *  <li>update any items that match some criteria.</li>
 *  <li>remove an items that match some criteria</li>
 * </ul>
 * <p>
 * Accordingly, a TestDataContainer could be thought of as a class which helps simulators maintain 
 * a predictable state.  Typical implementations would include a container that exposes a predetermined 
 * list of test objects or a container that generates valid test data on the fly.
 */
public interface TestDataContainer<T> {
	
	 /**
	 * @return size of set. If greater than Integer.MAX_VALUE, returns -1
	 * (Java's internal collection size is limited to int)
	 */
	int getCount();
	
	/**
	 * Gets Type information for the elements in the Data Set.
	 * @return Class of T
	 */
	Class<T> getType();
	
	/**
	 * remove all data from the repository 
	 */
	void clear();
	
	/**
	 * add an item into the DataContainer
	 * @param item item to add
	 * @return the item if successful, or null if the insertion failed
	 */
	T insert(T item);
	
	/**
	 * Get the next item in the set.
	 * @return the next item or null if no more items exist
	 */
	T next();
	
	/**
	 * gets the next <i>n</i> items from the current position
	 * @param count the number of items to return
	 * @return Collection of T
	 */
	List<T> next(int count);
	
	/**
	 * Remove any matching items from the repository
	 * @param matchTest A lambda expression for identifying items to remove 
	 * @return Number of items affected
	 */
	int remove(Predicate<T> matchTest);
	
	/**
	 * Remove up to <i>limit</i> items from the repository
	 * @param matchTest A lambda expression for identifying items to remove 
	 * @param limit The upper limit to remove
	 * @return Number of items affected
	 */
	int remove(Predicate<T> matchTest, int limit);
	
	/**
	 * Retrieves all items for which lambda expression is true
	 * @param matchTest A lambda expression for identifying items to retrieve 
	 * @return Collection of T
	 */
	Collection<T> select(Predicate<T> matchTest);
	
	/**
	 * Retrieves all items for which lambda expression is true
	 * @param matchTest A lambda expression for identifying items to retrieve 
	 * @param limit The upper limit to retrieve
	 * @return Collection of T
	 */
	Collection<T> select(Predicate<T> matchTest, int limit);
	
	/**
	 * Retrieve the entire current Data Set as a read-only, ordered list<br/>
	 * <b>WARNING: The List which is returned does not support insert, remove or update,
	 * so DO NOT use this method for any inserts, deletions or update operations</b>
	 * @return AbstractList of T
	 */
	List<T> selectAll();
	
	/**
	 * 
	 * @param matchTest A lambda expression for identifying items to update 
	 * @param transformer A lambda expression for altering matched items
	 * @return Collection of T
	 */
	<V> Collection<T> update(Predicate<T> matchTest, BiFunction<Predicate<T>, V , Collection<T>> transformer);
}
