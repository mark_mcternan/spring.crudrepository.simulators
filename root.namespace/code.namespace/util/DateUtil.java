package com.datus.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	/**
	 * Gets today's Date component
	 * @return Calendar instance set to first millisecond of today
	 */
	public static Calendar today() {
		return DateUtil.dateFromTime(new Date());
	}

	/**
	 * Gets the date component of a given timestamp
	 * @param timestamp date and time to process
	 * @return date component of timestamp
	 */
	public static Date date(Date timestamp){
		return DateUtil.dateFromTime(timestamp).getTime();
	}
	
	private static Calendar dateFromTime(Date timestamp){
		Calendar c = Calendar.getInstance();
		c.setTime(timestamp);
		c.set(Calendar.HOUR_OF_DAY, 0);  
		c.set(Calendar.MINUTE, 0);  
		c.set(Calendar.SECOND, 0);  
		c.set(Calendar.MILLISECOND, 0); 
		return c;
	}
}
