package com.datus.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mark.mcternan on 12/27/2014.
 * Name was chosen to conform with DateUtil and avoid conflicts with other common StringUtils libraries
 */
public class StringUtil{
    // implementation of String.join for < Java 1.8
    public static String join(String[] source, String delimiter){

        // append first element if non-null or empty
        StringBuilder s = StringUtil.isNullOrEmpty(source[0]) ? new StringBuilder() : new StringBuilder(source[0]);

        // prepend delimiter, start at 2nd element
        for (int i = 1; i < source.length; i++) {
            if(!StringUtil.isNullOrEmpty(source[i])) {
                s.append(delimiter);
                s.append(source[i]);
            }
        }

        return s.toString();
    }

    // implementation of isNullOrEmpty for Java
    public static boolean isNullOrEmpty(String item){
        return (item == null || item.isEmpty());
    }
    
    /**
	 * Converts the string representation of a value to the provided type
	 * @param type The type of value to return.  
	 * Supported types include: Date, int [via long], long, float, 
	 * @param value The string representation of the value
	 * @return A converted value of the specified type.
	 */
	public static <V> V convertType(Class<V> type, String value){
		
		if(type.isAssignableFrom(String.class)){
			return !value.isEmpty() ? (V) value : null;
		}
		
		if(type.isAssignableFrom(Date.class)){
			DateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
			try {
				return (V) dt.parse(value);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if(type.isAssignableFrom(Boolean.class)){

			// string switch not supported prior to 1.7
			switch(value.toUpperCase()){
				case "1":
				case "T":
				case "Y":
				case "YES":
				case "TRUE":
					return (V) Boolean.TRUE;
				case "0":
				case "F":
				case "N":
				case "NO":
				case "FALSE":
				default:
					return (V) Boolean.FALSE;
			}
			
			// // string switch alternate
			// String val = value.toUpperCase();
			// if(val == "1" || val == "T" || val == "Y" || val == "YES" || val == "TRUE"){
			//	return (V) Boolean.TRUE;
			// } else {
			//	return (V) Boolean.FALSE;
			// }
		}
		
		if(type.isAssignableFrom(Long.class) || 
		   type.isAssignableFrom(long.class) || 
		   type.isAssignableFrom(Integer.class) ||
		   type.isAssignableFrom(int.class))
		{
			return (V) Long.valueOf(value);
		}
		
		if(type.isAssignableFrom(Float.class) ||
		   type.isAssignableFrom(float.class)){
			return (V) Float.valueOf(value);
		}
		
		return null;
	}
}
