package com.datus.test.service;

import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.datus.domain.Customer;
import com.datus.domain.CustomerOffer;
import com.datus.domain.TruAxisRedemptionResponse;
import com.datus.service.*;
import com.datus.simulators.MockCustomerOfferRepository;
import com.datus.simulators.MockCustomerRepository;

public class RedemptionServiceTests {

	@Test
	public void testRedemptionResponse_WithValidCustomerAndOffer_SetsOfferStatusToRedeemed() {
		
		// Arrange
		MockCustomerRepository customerrepo = new MockCustomerRepository(MockCustomerRepository.generateCustomers(1));
		MockCustomerOfferRepository offerrepo = new MockCustomerOfferRepository(MockCustomerOfferRepository.generateCustomerOffers(10));
		
		// get customer & offer, overwrite offer fields
		Customer c = customerrepo.findOne(new Long(1));
		String token = c.getTruAxisToken();
		CustomerOffer o = offerrepo.findOne(new Long(2));
		//o.setTruAxisOfferId(c.getTruAxisToken());
		
		o.setId(c.getId());
		o.setCustomerId(c.getId());
		o.setTruAxisOfferId(c.getId());
		
		// setup response
		TruAxisRedemptionResponse response = new TruAxisRedemptionResponse();
		response.setUserToken(c.getTruAxisToken());
		response.setOfferId(o.getTruAxisOfferId().toString());
		
		// set up services
		CustomerService cs = new CustomerServiceImpl();
		ReflectionTestUtils.setField(cs, "customerRepository", customerrepo);
		
		RedemptionService rs = new RedemptionServiceImpl();
		ReflectionTestUtils.setField(cs, "customerRepository", customerrepo);
		ReflectionTestUtils.setField(rs, "customerOfferRepository", offerrepo);
		ReflectionTestUtils.setField(rs, "customerService", cs);
		
		// Act
		rs.redemptionResponse(response);
		
		// Assert
		org.junit.Assert.assertEquals("Failed to set redeem status", "REDEEMED", o.getStatus());
	}
	
	@Test
	public void testRedemptionResponse_WithInvalidCustomer_DoesNothing() {
		
		// Arrange
		MockCustomerRepository customerrepo = new MockCustomerRepository(MockCustomerRepository.generateCustomers(1));
		MockCustomerOfferRepository offerrepo = new MockCustomerOfferRepository(MockCustomerOfferRepository.generateCustomerOffers(10));
		
		// get customer & offer, overwrite offer fields
		Customer c = customerrepo.findOne(new Long(1));
		String token = c.getTruAxisToken();
		CustomerOffer o = offerrepo.findOne(new Long(2));
		//o.setTruAxisOfferId(c.getTruAxisToken());
		
		o.setId(c.getId());
		o.setCustomerId(c.getId());
		o.setTruAxisOfferId(c.getId());
		
		// setup response
		TruAxisRedemptionResponse response = new TruAxisRedemptionResponse();
		response.setUserToken(c.getTruAxisToken());
		//response.setOfferId(o.getTruAxisOfferId().toString());
		
		// set up services
		CustomerService cs = new CustomerServiceImpl();
		ReflectionTestUtils.setField(cs, "customerRepository", customerrepo);
		
		RedemptionService rs = new RedemptionServiceImpl();
		ReflectionTestUtils.setField(cs, "customerRepository", customerrepo);
		ReflectionTestUtils.setField(rs, "customerOfferRepository", offerrepo);
		ReflectionTestUtils.setField(rs, "customerService", cs);
		
		// Act
		rs.redemptionResponse(response);
		
		// Assert
		org.junit.Assert.assertNotEquals("status should not have been updated", "REDEEMED", o.getStatus());
	}

}
