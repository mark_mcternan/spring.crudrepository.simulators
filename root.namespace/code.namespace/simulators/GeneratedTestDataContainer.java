/**
 * 
 */
package com.datus.simulators;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mbm
 * <p>
 * Test Data Container that generates test data from a template.
 */
public class GeneratedTestDataContainer<T> extends PreloadedTestDataContainer<T>
{
	/** constructor generates list based on template
	 * @param type The type of Elements to be returned.  (used for reflection after type erasure).
	 * @param template An example of T with default values populated.
	 * @param generators An array of FieldValueGenerators for generating field data
	 * @param quantityToGenerate Number of Instances to generate
	 */
	public GeneratedTestDataContainer(Class<T> type, FieldValueGenerator[] generators, long quantityToGenerate){
		super(type, GeneratedTestDataContainer.generateListFromTemplate(type, generators, quantityToGenerate));
	}

	/**
	 * static method for generating multiple instances T based on a set of FieldValueGenerators.
	 * @param type The type of Elements to be created.  T *MUST* expose a parameterless constructor.
	 * @param generators An array of FieldValueGenerators for generating field data
	 * @param quantityToGenerate Number of Instances to generate
	 * @return List of generated Items of type T
	 */
	private static <T> List<T> generateListFromTemplate(Class<T> type, FieldValueGenerator[] generators, long quantityToGenerate){
		
		List<T> output = new ArrayList<T>();

		try {
			// get parameterless constructor and set accessible
			Constructor<T> ctor = type.getDeclaredConstructor((Class<?>[]) null);
			ctor.setAccessible(true);
			
			// load generators
			for(long i = 0; i < quantityToGenerate; i++){
				
				// instantiate new copy
				T copy = ctor.newInstance((Object[]) null);
				
				// populate fields
				for(int j = 0; j < generators.length; j++){
   					generators[j].generate(type, copy);
				}
				output.add(copy);
			}
			
			return output;
			
		} catch (IllegalArgumentException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			System.out.printf("Failed to instantiate type '%s'.\n", type.getName());
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			System.out.printf("Parameterless constructor for type '%s' is required and does not exist.\n", type.getName());
		}
		
		return null;
	}
	
//	/**
//	 * Because we cannot constrain Generic's to have a parameterless constructor that can be 
//	 * instantiated directly, we must have a means to new up a new instance of a generic class
//	 * and copy field values from one to the new instance.
//	 * @param type the type of object to clone
//	 * @param template the source object, which must have a public, parameterless constructor
//	 * @return a shallow copy of the class
//	 */
//	private static <T> T cloneObject(Class<T> type, T template){
//		
//		try {
//			T copy = type.newInstance();
//			
//			// copy each field found in template
//			Field[] flds = type.getDeclaredFields();
//			for(int i = 0; i < flds.length; i++){
//				
//				// test if field is volatile
//				if((flds[i].getModifiers() & Modifiers.STATIC.getBit()) == 0){
//					flds[i].setAccessible(true);
//					Object o = flds[i].get(template);
//					if(o != null){
//						flds[i].set(copy, o);
//					}
//				}
//			}
//			
//			return  copy;
//			
//		} catch (InstantiationException | IllegalAccessException e) {
//			e.printStackTrace();
//			return (T) null;
//		}
//	}
}
